@echo off
setlocal enabledelayedexpansion enableextensions
title RanaSH
:: Introduction.
echo Rana's SHell (RanaSH) is a front-end to the Microsoft command prompt.
echo Made by JadeAsh: https://0xacab.org/jadeash/ranash
echo.
echo Additions include:
echo "ranash", and all of its uses.
echo The ability to use typically batch-only commands while in an interactive environment.
echo.
:: Introduction over.

:mainsh
:: Prompts user for input, while also formatting itself as close as UNIX's sh, while remaining fairly simple.
set /p rsh="!username!@!userdomain!: !cd! $ "
:: Checks for all commands entered that have the prefix "ranash", and sends them to later in the file.
if "!rsh!"=="ranash*" goto rsh
:: Formatting, and running that command we got earlier.
echo.
!rsh!
echo.
goto mainsh


:rsh
:: Not certain if this works. I will test it later, when I am on Windows.
set rsh="!rsh:-7!"
if "!rsh!"=="" goto rhelp
if "!rsh!"=="-h" goto rhelp
if "!rsh!"=="--help" goto rhelp
if "!rsh!"=="--test" goto rtest
goto mainsh

:rhelp
echo.
echo Hello, and welcome to Rana's SHell. Here's a list of commands.
echo.
echo -h --help - Displays this wonderful, and hopefully helpful, wall of text.
echo --test - Does a very simplistic test. 
echo.
goto mainsh

:rtest
echo.
echo This is a test.
set rtest="Test^! Look at it."
echo !rtest!
echo.
goto mainsh
